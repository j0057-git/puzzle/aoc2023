# Advent of Code 2023

My solutions to [Advent of Code 2023][aoc2023] using Python and [pytest-aoc][].

[aoc2023]: https://adventofcode.com/2023
[pytest-aoc]: https://pypi.org/project/pytest-aoc/

## Setting up and running the code

Create a venv: `python -m venv env`.

Allow direnv to execute the .envrc: `direnv allow`, or `. .envrc` if you don't
have direnv (you should).

Install dependencies: `pip install -e .[test]`.

Run tests: `pytest`.
