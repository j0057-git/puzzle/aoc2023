from collections import Counter
from enum import IntEnum

Hand = IntEnum('Hand', 'HIGH_CARD ONE_PAIR TWO_PAIR THREE_KIND FULL_HOUSE FOUR_KIND FIVE_KIND')

def parse(grid):
    return [(tuple('__23456789TJQKA'.index(c) for c in C), int(b)) for (C, b) in grid]

def score(H):
    match [n for (_, n) in Counter(H).most_common()]:
        case [5]:        return Hand.FIVE_KIND
        case [4, *_]:    return Hand.FOUR_KIND
        case [3, 2]:     return Hand.FULL_HOUSE
        case [3, *_]:    return Hand.THREE_KIND
        case [2, 2, *_]: return Hand.TWO_PAIR
        case [2, *_]:    return Hand.ONE_PAIR
        case [1, *_]:    return Hand.HIGH_CARD

def apply_jokers(S, H):
    match (sum(c == 1 for c in H), S):
        case (5, Hand.FIVE_KIND):  return Hand.FIVE_KIND
        case (1, Hand.FOUR_KIND):  return Hand.FIVE_KIND
        case (4, Hand.FOUR_KIND):  return Hand.FIVE_KIND
        case (2, Hand.FULL_HOUSE): return Hand.FIVE_KIND
        case (3, Hand.FULL_HOUSE): return Hand.FIVE_KIND
        case (1, Hand.THREE_KIND): return Hand.FOUR_KIND
        case (3, Hand.THREE_KIND): return Hand.FOUR_KIND
        case (1, Hand.TWO_PAIR):   return Hand.FULL_HOUSE
        case (2, Hand.TWO_PAIR):   return Hand.FOUR_KIND
        case (1, Hand.ONE_PAIR):   return Hand.THREE_KIND
        case (2, Hand.ONE_PAIR):   return Hand.THREE_KIND
        case (1, Hand.HIGH_CARD):  return Hand.ONE_PAIR
        case (0, h):               return h
        case (j, h):               raise ValueError(f"Can't apply {j} joker(s) for {H}, a {h!r}")

def day07a(R):
    S = sorted((score(H), H, b) for (H, b) in R)
    return sum((i+1) * b for (i, (_, _, b)) in enumerate(S))

def day07b(R):
    R = [([1 if c == 11 else c for c in H], b) for (H, b) in R]
    S = sorted((apply_jokers(score(H), H), H, b) for (H, b) in R)
    return sum((i+1) * b for (i, (_, _, b)) in enumerate(S))

def test_07_ex1(day07_ex_grid): assert day07a(parse(day07_ex_grid(0))) == 6440
def test_07_ex2(day07_ex_grid): assert day07b(parse(day07_ex_grid(0))) == 5905

def test_07a(day07_grid): assert day07a(parse(day07_grid)) == 249638405
def test_07b(day07_grid): assert day07b(parse(day07_grid)) == 249776650
