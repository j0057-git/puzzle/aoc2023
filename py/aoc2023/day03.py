import re
from math import prod

def parse(L):
    return [(y, m.start(), m.end(), int(m.group())) for (y, s) in enumerate(L)
                                                    for m in re.finditer(r'\d+', s)], \
           [(y, x, c) for (y, s) in enumerate(L)
                      for (x, c) in enumerate(s)
                      if not c.isdigit() and not c == '.'], \
           len(L), \
           len(L[0])

def adjacent(y1, s, e, h, w):
    return {(y2, x) for dy in [-1, 0, +1]
                    for x in range(s-1, e+1)
                    if 0 <= (y2 := y1 + dy) < h
                    if 0 <= x < w
                    if (dy != 0) or not (s <= x < e)}

def part_numbers(N, S, A):
    return (n for (y1, s, e, n) in N
              if any((y2, x2) in S for (y2, x2) in A(y1, s, e)))

def gear_ratios(G, N, A):
    return (prod(g) for (y, x) in G
                    if len(g := {N[c] for c in A(y, x) if c in N}) == 2)

def day03a(N, S, h, w):
    return sum(part_numbers(N, {(y, x) for (y, x, _) in S},
                            lambda y, s, e: adjacent(y, s, e, h, w)))

def day03b(N, S, h, w):
    return sum(gear_ratios([(y, x) for (y, x, c) in S if c == '*'],
                           {(y, x): n for (y, s, e, n) in N
                                      for x in range(s, e)},
                           lambda y, x: adjacent(y, x, x+1, h, w)))

def test_03_ex1(day03_ex_lines): assert day03a(*parse(day03_ex_lines(0))) == 4361
def test_03_ex2(day03_ex_lines): assert day03b(*parse(day03_ex_lines(0))) == 467835

def test_03a(day03_lines): assert day03a(*parse(day03_lines)) == 527446
def test_03b(day03_lines): assert day03b(*parse(day03_lines)) == 73201705
