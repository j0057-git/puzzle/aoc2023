from . import compose

W = ['one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine']

def replace_num(s, W):
    for i in range(len(s)):
        if s[i].isdigit():
            return s
        for j, w in enumerate(W):
            if s[i:].startswith(w):
                return s[:i] + str(j+1) + s[i+len(w):]
    return s

def replace_num_fwd(s):
    return replace_num(s, W)

def replace_num_rev(s):
    return replace_num(s[::-1], [w[::-1] for w in W])[::-1]

def value(v):
    return int(next(c for c in v if c.isdigit()) + next(c for c in reversed(v) if c.isdigit()))

def day01a(V):
    return sum(value(v) for v in V)

def day01b(V):
    return sum(compose(replace_num_fwd, replace_num_rev, value)(v) for v in V)

def test_01_ex1(day01_ex_lines): assert day01a(day01_ex_lines(0)) == 142
def test_01_ex2(day01_ex_lines): assert day01b(day01_ex_lines(1)) == 281

def test_01a(day01_lines): assert day01a(day01_lines) == 53651
def test_01b(day01_lines): assert day01b(day01_lines) == 53894
