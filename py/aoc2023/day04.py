from functools import reduce

def parse(lines):
    cards = [line.split(': ')[1].split(' | ') for line in lines]
    return [{int(n) for n in win.split()} for (win, _) in cards], \
           [{int(n) for n in have.split()} for (_, have) in cards]

def counts(win, have):
    return [len(w & h) for (w, h) in zip(win, have)]

def total_cards(wins):
    return reduce(lambda T, c: T + [1 + sum((wins[c-i-1] >= (i + 1)) * t for (i, t) in enumerate(T[-10:][::-1]))],
                  range(len(wins)), [])

def day04a(win, have):
    return sum(1 << (c-1) if c else 0 for c in counts(win, have))

def day04b(win, have):
    wins = counts(win, have)
    total = total_cards(wins)
    return sum(total)

def test_04_ex1(day04_ex_lines): assert day04a(*parse(day04_ex_lines(0))) == 13
def test_04_ex2(day04_ex_lines): assert day04b(*parse(day04_ex_lines(0))) == 30

def test_04a(day04_lines): assert day04a(*parse(day04_lines)) == 20107
def test_04b(day04_lines): assert day04b(*parse(day04_lines)) == 8172507

# c w o122333344444444    t
# 1 4 1............... =  1   1                                                                                 =  1 = 1
# 2 2 11.............. =  2   1 + (4 >= 1) *  1                                                                 =  2 = 1 + 1*1
# 3 2 1111............ =  4   1 + (2 >= 1) *  2 + (4 >= 2) * 1                                                  =  4 = 1 + 1*2 + 1*1
# 4 1 11111111........ =  8   1 + (2 >= 1) *  4 + (2 >= 2) * 2 + (4 >= 3) * 1                                   =  8 = 1 + 1*4 + 1*2 + 1*1
# 5 0 11..111111111111 = 14   1 + (1 >= 1) *  8 + (2 >= 2) * 4 + (2 >= 3) * 2 + (4 >= 4) * 1                    = 14 = 1 + 1*8 + 1*4 + 0*2 + 1*1
# 6 0 1............... =  1   1 + (0 >= 1) * 14 + (1 >= 2) * 8 + (2 >= 3) * 4 + (2 >= 4) * 2 + (4 >= 5) * 1     =  1 = 1 + 0*14+ 0*8 + 0*4 + 0*2 + 0*1
