from functools import reduce
from itertools import takewhile, pairwise, chain, count

compose = lambda *F: reduce(lambda f, g: lambda a: g(f(a)), F)

stopwhen = lambda P, I: (x for (_, (_, x)) in takewhile(lambda t: not t[0] or not P(t[1][0]), enumerate(pairwise(chain([None], I)))))

gcd = lambda a, b: a if b == 0 else gcd(b, a % b)
lcm = lambda a, b: abs(a * b) // gcd(a, b)

ilen = lambda I: reduce(lambda a, _: a+1, I, 0)

scan = lambda s, f: chain([s], (s := f(s) for _ in count()))
