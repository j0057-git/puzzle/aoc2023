from itertools import accumulate, cycle
from functools import reduce

from . import ilen, stopwhen, lcm

def parse(lines):
    return ['LR'.index(c) for c in lines[0]], \
           {n: e[1:-1].split(', ') for n, e in (s.split(' = ') for s in lines[2:])}

def walk(P, N, start, is_stop):
    return stopwhen(is_stop, accumulate(cycle(P), lambda c, p: N[c][p], initial=start))

def day08a(P, N):
    return ilen(walk(P, N, 'AAA', lambda n: n == 'ZZZ'))-1

def day08b(P, N):
    return reduce(lcm, (ilen(walk(P, N, s, lambda n: n.endswith('Z')))-1
                        for s in N if s.endswith('A')))

def test_08_ex1(day08_ex_lines): assert day08a(*parse(day08_ex_lines(0))) == 2
def test_08_ex2(day08_ex_lines): assert day08a(*parse(day08_ex_lines(1))) == 6
def test_08_ex3(day08_ex_lines): assert day08b(*parse(day08_ex_lines(2))) == 6

def test_08a(day08_lines): assert day08a(*parse(day08_lines)) == 16043
def test_08b(day08_lines): assert day08b(*parse(day08_lines)) == 15726453850399
