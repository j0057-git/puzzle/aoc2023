from itertools import combinations

manhattan = lambda x1, y1, x2, y2: abs(x1-x2) + abs(y1-y2)

def parse(lines):
    assert (D := len(lines[0])) == len(lines)
    return D, {(x, y) for (y, s) in enumerate(lines)
                      for (x, c) in enumerate(s)
                      if c == '#'}

def expand(D, G, f=2):
    C = [c for c in range(D) if all(x != c for (x, _) in G)]
    R = [r for r in range(D) if all(y != r for (_, y) in G)]
    return {(x - (E := sum(x > c for c in C)) + f * E,
             y - (E := sum(y > r for r in R)) + f * E) for (x, y) in G}

def day11a(D, G):
    return sum(manhattan(*a, *b) for (a, b) in combinations(expand(D, G), 2))

def day11b(D, G, f=1000000):
    return sum(manhattan(*a, *b) for (a, b) in combinations(expand(D, G, f), 2))

def test_11_ex1(day11_ex_lines): assert day11a(*parse(day11_ex_lines(0))) == 374
def test_11_ex2(day11_ex_lines): assert day11b(*parse(day11_ex_lines(0)), f=10) == 1030
def test_11_ex3(day11_ex_lines): assert day11b(*parse(day11_ex_lines(0)), f=100) == 8410

def test_11a(day11_lines): assert day11a(*parse(day11_lines)) == 10165598
def test_11b(day11_lines): assert day11b(*parse(day11_lines)) == 678728808158
