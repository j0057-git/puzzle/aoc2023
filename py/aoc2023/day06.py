from math import sqrt, floor, ceil, prod

def parse(lines):
    return [*zip((int(t) for t in lines[0][5:].split()),
                 (int(d) for d in lines[1][9:].split()))]

def quad_eq(a, b, c):
    return [(-b + s*sqrt(b*b - 4*a*c)) / 2*a for s in [+1, -1]]

def count_ways(t, d):
    t1, t2 = quad_eq(-1, t, -d-1)
    return floor(t2) - ceil(t1) + 1

def day06a(races):
    return prod(count_ways(t, d) for (t, d) in races)

def day06b(races):
    return count_ways(int(''.join(str(t) for (t, _) in races)),
                      int(''.join(str(d) for (_, d) in races)))

def test_06_ex1(day06_ex_lines): assert day06a(parse(day06_ex_lines(0))) == 288
def test_06_ex2(day06_ex_lines): assert day06b(parse(day06_ex_lines(0))) == 71503

def test_06a(day06_lines): assert day06a(parse(day06_lines)) == 2449062
def test_06b(day06_lines): assert day06b(parse(day06_lines)) == 33149631
