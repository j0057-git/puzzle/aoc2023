from functools import reduce

def parse(lines):
    (seeds, *maps) = lines.split('\n\n')
    return [int(n) for n in seeds.split()[1:]], \
           [sorted((f, f+c, t-f) for (t, f, c) in [[int(n) for n in m.split()] for m in M])
            for (t, *M) in [map_.split('\n') for map_ in maps]]

def location(n, maps):
    return reduce(lambda n, M: next((n + d for (s, e, d) in M if s <= n < e), n), maps, n)

def map_ranges(seeds, maps):
    while seeds:
        (s1, e1) = seeds.pop(0)
        for (s2, e2, d) in maps:
            if s1 < e1 <= s2 < e2: continue
            if s2 < e2 <= s1 < e1: continue
            if s2 <= s1 < e1 <= e2: # start+end in range
                yield (s1 + d, e1 + d) ; break
            elif s2 <= s1 < e2 <= e1: # start in range
                yield (s1 + d, e2 + d)
                seeds.insert(0, (e2, e1))
                break
            elif s1 < s2 <= e1 < e2: # end in range
                yield (s1, s2)
                seeds.insert(0, (s2, e2))
                break
            elif s1 < s2 < e2 < e1: # start+end NOT in range
                yield (s1, s2)
                seeds.insert(0, (s2, e2))
                seeds.insert(0, (e2, e1))
                break
            raise NotImplementedError
        else:
            yield (s1, e1)

def day05a(seeds, maps):
    return min(location(n, maps) for n in seeds)

def day05b(seeds, maps):
    seeds = [(seeds[i], seeds[i]+seeds[i+1]) for i in range(0, len(seeds), 2)]
    locations = reduce(lambda S, m: [*map_ranges(S, m)], maps, seeds)
    return min(s for (s, _) in locations)

def test_05_ex1(day05_ex_text): assert day05a(*parse(day05_ex_text(0))) == 35
def test_05_ex2(day05_ex_text): assert day05b(*parse(day05_ex_text(0))) == 46

def test_05a(day05_text): assert day05a(*parse(day05_text)) == 3374647
def test_05b(day05_text): assert day05b(*parse(day05_text)) == 6082852

# seed     |soil       |fert       |water      |light      |temp       |hum        |loc       |
#----------+-----------+-----------+-----------+-----------+-----------+-----------+----------+
#          |50-98 :  +2| 0-15: +39 | 0-7 : +42 |18-25: +70 |45-64 : +36| 0-69:   +1|56-93:  +4|
#          |98-100: -48|15-52: -15 | 7-11: +50 |25-95:  -7 |64-77 :  +4|69-70:  -69|93-97: -37|
#          |           |52-54: -15 |11-53: -11 |           |77-100: -32|           |          |
#          |           |           |53-61:  -4 |           |           |           |          |
#----------+-----------+-----------+-----------+-----------+-----------+-----------+----------+
# 79..93   |81..95     |81..95     |81..95     |74..88     |78..81     |78..81     |82..84    |
#          |           |           |           |           |45..56     |46..57     |46..55    |
#          |           |           |           |           |           |           |60..61    |
# 55..68   |57..70     |57..70     |53..57     |46..50     |82..86     |82..86     |87..90    |
#          |           |           |61..70     |54..63     |90..99     |90..99     |94..97    |
#                                                                                  |56..60    |
#                                                                                  |97..99    |
