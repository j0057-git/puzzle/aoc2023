from math import prod

def parse(P):
    return [[{c: int(n) for (n, c) in [s.split() for s in S.split(', ')]}
             for S in G.split('; ')]
            for G in [p.split(': ')[1] for p in P]]

def possible(G, M):
    return [i+1 for i, Q in enumerate(G)
                if all(all(S.get(c, 0) <= n for c, n in M) for S in Q)]

def powers(G, C):
    return (prod(max(S.get(c, 0) for S in Q) for c in C)
            for Q in G)

def day02a(G):
    return sum(possible(G, [('red', 12), ('green', 13), ('blue', 14)]))

def day02b(G):
    return sum(powers(G, ['red', 'green', 'blue']))

def test_02_ex1(day02_ex_lines): assert day02a(parse(day02_ex_lines(0))) == 8
def test_02_ex2(day02_ex_lines): assert day02b(parse(day02_ex_lines(0))) == 2286

def test_02a(day02_lines): assert day02a(parse(day02_lines)) == 2685
def test_02b(day02_lines): assert day02b(parse(day02_lines)) == 83707
