from itertools import accumulate, pairwise

from . import stopwhen, scan

def next_val(seq):
    deltas = scan(seq, lambda S: [b-a for a,b in pairwise(S)])
    series = stopwhen(lambda S: all(x == 0 for x in S), deltas)
    return sum(d[-1] for d in series)

def day09a(grid):
    return sum(next_val(row) for row in grid)

def day09b(grid):
    return sum(next_val(row[::-1]) for row in grid)

def test_09_ex1(day09_ex_number_grid): assert day09a(day09_ex_number_grid(0)) == 114
def test_09_ex2(day09_ex_number_grid): assert day09b(day09_ex_number_grid(0)) == 2

def test_09a(day09_number_grid): assert day09a(day09_number_grid) == 1789635132
def test_09b(day09_number_grid): assert day09b(day09_number_grid) == 913
