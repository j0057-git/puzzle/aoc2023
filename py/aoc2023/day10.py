from functools import reduce
from itertools import takewhile

from . import scan, ilen

PIPES = {(+1, 0, '-'): (+1, 0), (0, +1, '|'): (0, +1),
         (-1, 0, '-'): (-1, 0), (0, -1, '|'): (0, -1),
         (0, +1, 'J'): (-1, 0), (0, +1, 'L'): (+1, 0), (0, -1, '7'): (-1, 0), (0, -1, 'F'): (+1, 0),
         (+1, 0, 'J'): (0, -1), (-1, 0, 'L'): (0, -1), (+1, 0, '7'): (0, +1), (-1, 0, 'F'): (0, +1)}

def parse(G):
    return next((x, y) for y, s in enumerate(G) if (x := s.find('S')) > -1), \
            {(x, y): c for y, s in enumerate(G)
                       for x, c in enumerate(s)}

def follow(S, G):
    d = next((dx, dy) for dx, dy in [(+1, 0), (0, +1), (-1, 0), (0, -1)] if (dx, dy, G[S[0]+dx, S[1]+dy]) in PIPES)
    P = scan(S+d, lambda t: (t[0]+t[2], t[1]+t[3]) + PIPES.get((t[2], t[3], G[t[0]+t[2], t[1]+t[3]]), (0, 0)))
    return takewhile(lambda t: t[-2:] != (0, 0), P)

def day10a(S, G):
    return ilen(follow(S, G)) // 2

def test_10_ex1(day10_ex_lines): assert day10a(*parse(day10_ex_lines(1))) == 4
def test_10_ex2(day10_ex_lines): assert day10a(*parse(day10_ex_lines(2))) == 4
def test_10_ex3(day10_ex_lines): assert day10a(*parse(day10_ex_lines(3))) == 8
def test_10_ex4(day10_ex_lines): assert day10a(*parse(day10_ex_lines(4))) == 8

def test_10a(day10_lines): assert day10a(*parse(day10_lines)) == 6867
